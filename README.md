AsplAdminBundle
=======================

This bundle has a ready - startup admin based on which backend admin as per requirement can be made easily.

Features
------------

* Admin theme with Bootstrap 3
* Responsive
* Dynamic admin menu and its management
* Simple admin user management
* Access roles and permissions
* Searching / Sorting / Paging in list
* Session based searching / sorting / paging so it retains its stat per entity throughout admin
* Flash message service
* One can set this as base and extend admin as per requirement

Please see INSTALL.md for installation.

Mail me at hiren@aspl.in if any suggestion / issue with installation / pull request.

Thanks!
