Version 1.0.3
--
* Code optimization of repository class
* Fix my account redirection in accordance with permission
* Add permission to self object in user for my account
* Hide menu/button of which no access is given to user
* More code optimization at required places

Version 1.0.4
--
* Status swap functionality in listing
* Site settings - admin name, admin email, no of records per page, from name, from email, copyright sentence
* Multilangual
* Mail notification when user is added/changed