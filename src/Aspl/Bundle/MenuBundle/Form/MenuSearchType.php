<?php
namespace Aspl\Bundle\MenuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Aspl\Bundle\MenuBundle\Form\DataTransformer\MenuToIdTransformer;

use Doctrine\ORM\EntityRepository;

class MenuSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];

        $transformer = new MenuToIdTransformer($entityManager);

        $builder
        ->add('caption', 'text', array(
                'required' => false,
        ))
        ->add($builder->create('parent', 'entity', array(
                'required' => false,
                'class' => 'MenuBundle:Menu',
                'data_class' => 'Aspl\Bundle\MenuBundle\Entity\Menu',
                'property' => 'caption',
                'empty_value' => 'Parent',
                'query_builder' => function(EntityRepository $er) {
                return $er->getParentQueryBuilder();
        },
        ))->addModelTransformer($transformer))
        ->add('status', 'choice', array(
            'required' => false,
            'choices' => array('1' => 'Active', '0' => 'Inactive'),
            'multiple' => false,
            'expanded' => true,
            'empty_value' => false,
        ))
        ->add('search', 'submit')
        ->add('reset', 'submit');
    }

    public function getName()
    {
        return 'search';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
        ->setDefaults(array(
                'data_class' => 'Aspl\Bundle\MenuBundle\Entity\Menu'
        ))
        ->setRequired(array(
                'em',
        ))
        ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));;
    }
}