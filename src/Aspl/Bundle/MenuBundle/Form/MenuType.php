<?php

namespace Aspl\Bundle\MenuBundle\Form;

use Aspl\Bundle\MenuBundle\Entity\MenuRepository;

use Aspl\Bundle\MenuBundle\Entity\Menu;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\Type as AssertType;
use Symfony\Component\Validator\Constraints\NotBlank;

use Doctrine\ORM\EntityRepository;

use Aspl\Bundle\MenuBundle\Form\DataTransformer\MenuToIdTransformer;

class MenuType extends AbstractType
{
    protected $entityManager;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['em'];

        $transformer = new MenuToIdTransformer($this->entityManager);

        $builder
            ->add('caption', 'text', array(
                                        'required' => false,
                                        'constraints' => array(
                                                            new NotBlank(array('message' => 'Please enter caption.')),
                                                         )))
            ->add('link', 'text', array(
                                        'required' => false,
                                        'constraints' => array(
                                                            new NotBlank(array('message' => 'Please enter link.')),
                                                         )))
            ->add($builder->create('parent', 'choice', array(
                                                            'required' => false,
                                                            'choices' => $this->getParentMenu(),
                                                       ))->addModelTransformer($transformer))
            ->add('iconClass', 'text', array('required' => false))
            ->add('entityClass', 'text', array('required' => false))
            ->add('ord', 'number', array(
                                    'required' => false,
                                    'label' => 'Order',
                                    'precision' => 'integer'))
            ->add('status', 'choice', array(
                                        'required' => false,
                                        'choices' => array('1' => 'Active', '0' => 'Inactive'),
                                        'multiple' => false,
                                        'expanded' => true,
                                        'empty_value' => false,
                                        'constraints' => array(
                                                            new NotBlank(array('message' => 'Please select status.')),
                                                         )))
            ->add('save', 'submit')
            ->add('saveAndNew', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
            'data_class' => 'Aspl\Bundle\MenuBundle\Entity\Menu'
            ))
            ->setRequired(array(
                'em'
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aspl_bundle_menubundle_menu';
    }

    protected function getParentMenu()
    {
        $parentMenu = array();

        $menus = $this->entityManager
            ->getRepository('MenuBundle:Menu')
            ->getParent();

        foreach($menus as $menu) {
            $parentMenu[$menu->getId()] = $menu->getCaption();
        }

        return $parentMenu;
    }
}
