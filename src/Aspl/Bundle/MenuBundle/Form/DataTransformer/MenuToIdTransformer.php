<?php
namespace Aspl\Bundle\MenuBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Aspl\Bundle\MenuBundle\Entity\Menu;

class MenuToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (menu) to a string (id).
     *
     * @param  Menu|null $menu
     * @return integer
     */
    public function transform($menu)
    {
        if (null === $menu) {
            return "";
        }

        return $menu->getId();
    }

    /**
     * Transforms a string (id) to an object (menu).
     *
     * @param  string $id
     *
     * @return Menu|null
     *
     * @throws TransformationFailedException if object (menu) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $menu = $this->om
        ->getRepository('MenuBundle:Menu')
        ->findOneBy(array('id' => $id))
        ;

        if (null === $menu) {
            throw new TransformationFailedException(sprintf(
                    'An issue with id "%s" does not exist!',
                    $id
            ));
        }

        return $menu;
    }
}