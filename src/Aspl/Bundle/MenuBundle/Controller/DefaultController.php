<?php

namespace Aspl\Bundle\MenuBundle\Controller;

use Aspl\Bundle\UserBundle\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Aspl\Bundle\MenuBundle\Entity\Menu;
use Aspl\Bundle\MenuBundle\Form\MenuType;
use Aspl\Bundle\MenuBundle\Form\MenuSearchType;

use Symfony\Component\HttpFoundation\Request;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    /**
     * @Route("/menus", name="_menu", host="%host_admin%")
     * @Template()
     */
    public function indexAction()
    {
        try {
            $this->get('acl.manager')->isGrantedToView();

            $this->get('list.manager')->init($this->getDoctrine()->getRepository('MenuBundle:Menu'));

            $form = $this->createForm(new MenuSearchType(), null, array(
                    'em' => $this->getDoctrine()->getManager(),
                    'action' => $this->generateUrl('_menu'),
            ));

            $data = $this->get('list.manager')->getData();

            if ($data['search'])
                $form->submit($data['search']);

            return array(
                    'heading' => 'Menus',
                    'form' => $form->createView(),
                    'pagerfanta' => $this->get('list.manager')->getPagerfanta(),
                    'sorter' => $data['sorter'],
            );
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_dashboard'));
        }
    }

    /**
     * @Route("/menu/new", name="_menu_new", host="%host_admin%")
     * @Template("MenuBundle:Default:form.html.twig")
     */
    public function newAction(Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToCreate();

            $form = $this->createForm(new MenuType(), new Menu(), array(
                    'em' => $this->getDoctrine()->getManager(),
            ));

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setAddMessage('Menu');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_menu_new' : '_menu'));
            }

            return array('form' => $form->createView(), 'heading' => 'New menu');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_menu'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_menu'));
        }
    }

    /**
     * @Route("/menu/edit/{id}", name="_menu_edit", host="%host_admin%")
     * @Template("MenuBundle:Default:form.html.twig")
     */
    public function editAction($id, Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToEdit();

            $menu = $this->getDoctrine()
            ->getRepository('MenuBundle:Menu')
            ->find($id);

            if (!$menu) {
                throw $this->createNotFoundException(
                        'No menu found for id '.$id
                );
            }

            $form = $this->createForm(new MenuType(), $menu, array(
                    'em' => $this->getDoctrine()->getManager(),
            ));

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setSaveMessage('Menu');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_menu_new' : '_menu'));
            }

            return array('form' => $form->createView(), 'heading' => 'Edit menu');
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Menu');

            return $this->redirect($this->generateUrl('_menu'));
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_menu'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_menu'));
        }
    }

    /**
     * @Route("/menu/delete/{id}", name="_menu_delete", host="%host_admin%")
     * @Template()
     */
    public function deleteAction($id)
    {
        $deleted = false;

        try {
            $this->get('acl.manager')->isGrantedToDelete();

            $menu = $this->getDoctrine()
            ->getRepository('MenuBundle:Menu')
            ->find($id);

            if (!$menu) {
                throw $this->createNotFoundException(
                        'No menu found for id '.$id
                );
            }

            $this->_delete($menu);

            $this->get('message.manager')->setDeleteMessage('Menu');

            $deleted = true;
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Menu');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();
        }
        catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('message.manager')->setErrorMessage(6);
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();
        }

        return $this->redirect($this->generateUrl('_menu', $deleted ? array('deleted' => true) : array()));
    }

    /**
     * @Route("/menu/render", name="_menu_render", host="%host_admin%")
     * @Template()
     */
    public function renderMenuAction($currentRoute)
    {
        return array('menus' => $this->_getParentMenu(), 'currentRoute' => $currentRoute);
    }

    /**
     * @Route("/dashboard", name="_dashboard", host="%host_admin%")
     * @Template()
     */
    public function dashboardAction()
    {
        return array('menus' => $this->_getParentMenu());
    }

    protected function _save(\Symfony\Component\Form\Form $form, Request $request)
    {
        try {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $menu = $form->getData();

                $em = $this->getDoctrine()->getManager();

                $em->persist($menu);

                $em->flush();

                return TRUE;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _delete(Menu $menu)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush();
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _getParentMenu()
    {
        return $this->getDoctrine()
        ->getRepository('MenuBundle:Menu')
        ->getParent();
    }
}