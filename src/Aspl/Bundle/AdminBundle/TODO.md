Version 1.0
a) Host entry in routes - DONE
b) Self referencing menu edit issue - DONE
c) Password should be filled already in edit - DONE
d) Confirm password field in user form - DONE
e) Parameters in plugin - services.xml - DONE
f) prepare README.md
g) Grammer change of menus - DONE

Version 2.0
a) Code optimization of repository class/menu form class
b) Status swap functionality in listing

Version 3.0
a) Pluging user group and symfony permission with admin
b) Site settings - admin name, admin email, no of records per page, from name, from email, copyright sentence
c) Mail notification when user is added/changed