<?php
namespace Aspl\Bundle\AdminBundle\Handler;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ListManager
{
    protected $requestStack;
    protected $session;
    protected $queryBuilder;
    protected $repository;

    public function __construct(RequestStack $requestStack, Session $session)
    {
        $this->requestStack = $requestStack;
        $this->session = $session;

        $this->session->start();

        $this->setData();
    }

    public function init($repository)
    {
        $this->setRepository($repository);

        $this->setData();

        $this->setQueryBuilder($this->prepareQueryBuilder());
    }

    protected function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function setQueryBuilder(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    protected function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    protected function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    protected function getSession()
    {
        return $this->session;
    }

    protected function getCurrentRoute()
    {
        return $this->getRequest()->get('_route');
    }

    protected function getKey()
    {
        return $this->getCurrentRoute();
    }

    protected function setData()
    {
        $data = $this->getAllData();

        $key = $this->getKey();

        $data[$key] = array(
                'sorter' => $this->getSorterData(),
                'pager' => $this->getPagerData(),
                'search' => $this->getSearchData(),
        );

        $this->getSession()->set('list_manager', serialize($data));
    }

    protected function getAllData()
    {
        return $this->getSession()->get('list_manager') ? unserialize($this->getSession()->get('list_manager')) : null;
    }

    public function getData($subkey=null)
    {
        $data = $this->getAllData();

        $key = $this->getKey();

        return $subkey ? (isset($data[$key][$subkey]) ? $data[$key][$subkey] : null) : (isset($data[$key]) ? $data[$key] : null);
    }

    protected function getSorterData()
    {
        $data = $this->getData('sorter');

        return array(
                'field' => $this->getRequest()->get('field', (isset($data['field']) && $data['field']) ? $data['field'] : 'id'),
                'sort' => $this->getRequest()->get('sort', (isset($data['sort']) && $data['sort']) ? $data['sort'] : 'desc'),
        );
    }

    protected function getPagerData()
    {
        $data = $this->getData('pager');

        return array('page' => $this->getRequest()->get('page', (isset($data['page']) && $data['page'] && !$this->doResetPage()) ? $data['page'] : 1));
    }

    protected function doResetPage()
    {
        return ($this->getRequest()->get('search') || $this->getRequest()->get('reset') || $this->getRequest()->get('deleted'));
    }

    protected function getSearchData()
    {
        $data = $this->getData('search');

        $searchData = $this->getRequest()->get('search', (isset($data) && $data) ? $data : array());

        return isset($searchData['reset']) ? array() : $searchData;
    }

    protected function prepareQueryBuilder()
    {
        return $this->getRepository()->getQueryBuilder($this->getData());
    }

    protected function getQuery()
    {
        return $this->getQueryBuilder()->getQuery();
    }

    public function getPagerfanta()
    {
        $adapter = new DoctrineORMAdapter($this->getQuery());

        $pagerfanta = new Pagerfanta($adapter);

        $pagerData = $this->getData('pager');

        $pagerfanta->setCurrentPage($pagerData['page']);

        return $pagerfanta;
    }
}