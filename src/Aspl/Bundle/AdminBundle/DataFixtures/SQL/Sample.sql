-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2014 at 01:04 PM
-- Server version: 5.1.39
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony_sample`
--

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent`, `caption`, `link`, `ord`, `status`, `created`, `updated`, `icon_class`, `entity_class`) VALUES
(5, NULL, 'Dashboard', '_dashboard', 10000, 1, '2014-07-14 17:47:32', '2014-07-15 13:09:16', 'fa-dashboard', NULL),
(6, NULL, 'Menus', '_menu', 9000, 1, '2014-07-14 17:48:08', '2014-07-30 11:41:37', 'fa-bars', 'Aspl\\Bundle\\MenuBundle\\Entity\\Menu'),
(7, 6, 'Menu Manager', '_menu', 200, 1, '2014-07-30 18:00:42', '2014-07-30 18:01:36', NULL, NULL),
(8, 6, 'Add New Menu', '_menu_new', 100, 1, '2014-07-14 17:49:51', '2014-07-14 17:49:51', NULL, NULL),
(12, NULL, 'Roles', '_role', 6000, 1, '2014-07-14 17:52:00', '2014-07-30 11:41:21', 'fa-group', 'Aspl\\Bundle\\UserBundle\\Entity\\Role'),
(13, 12, 'Role Manager', '_role', 200, 1, '2014-07-14 17:52:18', '2014-07-23 16:48:15', NULL, NULL),
(14, 12, 'Add New Role', '_role_new', 100, 1, '2014-07-14 17:52:41', '2014-07-23 16:47:34', NULL, NULL),
(15, NULL, 'Users', '_user', 5000, 1, '2014-07-14 17:53:03', '2014-07-30 11:40:09', 'fa-group', 'Aspl\\Bundle\\UserBundle\\Entity\\User'),
(16, 15, 'User Manager', '_user', 200, 1, '2014-07-14 17:53:19', '2014-07-14 17:53:19', NULL, NULL),
(17, 15, 'Add New User', '_user_new', 100, 1, '2014-07-14 17:53:37', '2014-07-18 16:44:15', NULL, NULL),
(18, NULL, 'Logout', '_logout', 4000, 1, '2014-07-14 17:54:48', '2014-07-14 18:05:38', 'fa-sign-out', NULL),
(19, NULL, 'Permissions', '_permission', 8000, 1, '2014-07-24 11:33:40', '2014-07-30 11:41:05', 'fa-magic', 'Aspl\\Bundle\\UserBundle\\Entity\\Permission'),
(20, 19, 'Permission manager', '_permission', 200, 1, '2014-07-24 11:34:02', '2014-07-24 11:34:48', NULL, NULL),
(21, 19, 'Add new permission', '_permission_new', 100, 1, '2014-07-24 11:34:39', '2014-07-24 17:09:12', NULL, NULL),
(22, 6, 'Edit Menu', '_menu_edit', NULL, 0, '2014-07-30 15:15:27', '2014-07-30 15:15:27', NULL, NULL),
(23, 6, 'Delete Menu', '_menu_delete', NULL, 0, '2014-07-30 15:19:48', '2014-07-30 15:19:48', NULL, NULL),
(24, 19, 'Edit Permission', '_permission_edit', NULL, 0, '2014-07-30 15:20:41', '2014-07-30 15:20:41', NULL, NULL),
(25, 6, 'Delete Permission', '_permission_delete', NULL, 0, '2014-07-30 15:20:54', '2014-07-30 15:20:54', NULL, NULL),
(26, 12, 'Edit Role', '_role_edit', NULL, 0, '2014-07-30 15:21:18', '2014-07-30 15:36:22', NULL, NULL),
(27, 12, 'Delete Role', '_role_delete', NULL, 0, '2014-07-30 15:21:33', '2014-07-30 15:21:33', NULL, NULL),
(28, 15, 'Edit User', '_user_edit', NULL, 0, '2014-07-30 15:21:56', '2014-07-30 15:21:56', NULL, NULL),
(29, 15, 'Delete User', '_user_delete', NULL, 0, '2014-07-30 15:22:08', '2014-07-30 15:22:08', NULL, NULL),
(30, 12, 'Role Permission', '_role_permission', NULL, 0, '2014-07-30 15:39:20', '2014-07-30 15:39:20', NULL, NULL);

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `name`, `created`, `updated`) VALUES
(1, 'VIEW', '2014-07-24 11:37:13', '2014-07-24 11:37:13'),
(2, 'EDIT', '2014-07-24 11:37:21', '2014-07-24 11:37:21'),
(3, 'CREATE', '2014-07-24 11:37:26', '2014-07-24 11:37:26'),
(4, 'DELETE', '2014-07-24 11:37:33', '2014-07-24 11:37:33');

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `role`, `created`, `updated`, `data`) VALUES
(5, 'Super Admin', 'ROLE_SUPER_ADMIN', '2014-07-24 11:13:41', '2014-07-29 12:07:26', 'a:20:{s:5:"menu6";a:1:{i:0;i:6;}s:12:"permission61";a:1:{i:0;s:3:"6,1";}s:12:"permission62";a:1:{i:0;s:3:"6,2";}s:12:"permission63";a:1:{i:0;s:3:"6,3";}s:12:"permission64";a:1:{i:0;s:3:"6,4";}s:6:"menu19";a:1:{i:0;i:19;}s:13:"permission191";a:1:{i:0;s:4:"19,1";}s:13:"permission192";a:1:{i:0;s:4:"19,2";}s:13:"permission193";a:1:{i:0;s:4:"19,3";}s:13:"permission194";a:1:{i:0;s:4:"19,4";}s:6:"menu12";a:1:{i:0;i:12;}s:13:"permission121";a:1:{i:0;s:4:"12,1";}s:13:"permission122";a:1:{i:0;s:4:"12,2";}s:13:"permission123";a:1:{i:0;s:4:"12,3";}s:13:"permission124";a:1:{i:0;s:4:"12,4";}s:6:"menu15";a:1:{i:0;i:15;}s:13:"permission151";a:1:{i:0;s:4:"15,1";}s:13:"permission152";a:1:{i:0;s:4:"15,2";}s:13:"permission153";a:1:{i:0;s:4:"15,3";}s:13:"permission154";a:1:{i:0;s:4:"15,4";}}'),
(8, 'Admin', 'ROLE_ADMIN', '2014-08-01 11:33:26', '2014-08-01 11:33:51', 'a:20:{s:5:"menu6";a:0:{}s:12:"permission61";a:0:{}s:12:"permission62";a:0:{}s:12:"permission63";a:0:{}s:12:"permission64";a:0:{}s:6:"menu19";a:0:{}s:13:"permission191";a:0:{}s:13:"permission192";a:0:{}s:13:"permission193";a:0:{}s:13:"permission194";a:0:{}s:6:"menu12";a:1:{i:0;i:12;}s:13:"permission121";a:1:{i:0;s:4:"12,1";}s:13:"permission122";a:1:{i:0;s:4:"12,2";}s:13:"permission123";a:1:{i:0;s:4:"12,3";}s:13:"permission124";a:1:{i:0;s:4:"12,4";}s:6:"menu15";a:1:{i:0;i:15;}s:13:"permission151";a:1:{i:0;s:4:"15,1";}s:13:"permission152";a:1:{i:0;s:4:"15,2";}s:13:"permission153";a:1:{i:0;s:4:"15,3";}s:13:"permission154";a:1:{i:0;s:4:"15,4";}}');

--
-- Dumping data for table `role_menu_permission`
--

INSERT INTO `role_menu_permission` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(151, 5, 6, 1),
(152, 5, 6, 2),
(153, 5, 6, 3),
(154, 5, 6, 4),
(155, 5, 19, 1),
(156, 5, 19, 2),
(157, 5, 19, 3),
(158, 5, 19, 4),
(159, 5, 12, 1),
(160, 5, 12, 2),
(161, 5, 12, 3),
(162, 5, 12, 4),
(163, 5, 15, 1),
(164, 5, 15, 2),
(165, 5, 15, 3),
(166, 5, 15, 4),
(167, 8, 12, 1),
(168, 8, 12, 2),
(169, 8, 12, 3),
(170, 8, 12, 4),
(171, 8, 15, 1),
(172, 8, 15, 2),
(173, 8, 15, 3),
(174, 8, 15, 4);

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `status`, `created`, `updated`, `username`) VALUES
(2, 'Hiren', 'Chhatbar', 'hiren@aspl.in', 'test', 1, '2014-07-10 18:05:44', '2014-08-01 11:14:35', 'hiren@aspl.in'),
(3, 'Atul', 'Kamani', 'atul@aspl.in', 'test', 1, '2014-08-01 12:45:17', '2014-08-01 13:01:42', 'atul@aspl.in');

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(2, 5),
(3, 8);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
