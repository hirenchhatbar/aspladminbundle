<?php

namespace Aspl\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleMenuPermission
 *
 * @ORM\Table(name="role_menu_permission")
 * @ORM\Entity(repositoryClass="Aspl\Bundle\UserBundle\Entity\RoleMenuPermissionRepository")
 */
class RoleMenuPermission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="roleMenuPermissions")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")
     */

    protected $roles;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Aspl\Bundle\MenuBundle\Entity\Menu", inversedBy="roleMenuPermissions")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     */
    private $menus;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Permission", inversedBy="roleMenuPermissions")
     * @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
     */
    private $permissions;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roles
     *
     * @param integer $roles
     * @return RoleMenuPermission
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return integer
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set menus
     *
     * @param integer $menus
     * @return RoleMenuPermission
     */
    public function setMenus($menus)
    {
        $this->menus = $menus;

        return $this;
    }

    /**
     * Get menus
     *
     * @return integer
     */
    public function getMenus()
    {
        return $this->menus;
    }

    /**
     * Set permissions
     *
     * @param integer $permissions
     * @return RoleMenuPermission
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * Get permissions
     *
     * @return integer
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->menus = new \Doctrine\Common\Collections\ArrayCollection();
        $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add roles
     *
     * @param \Aspl\Bundle\UserBundle\Entity\Role $roles
     * @return RoleMenuPermission
     */
    public function addRole(\Aspl\Bundle\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Aspl\Bundle\UserBundle\Entity\Role $roles
     */
    public function removeRole(\Aspl\Bundle\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Add menus
     *
     * @param \Aspl\Bundle\MenuBundle\Entity\Menu $menus
     * @return RoleMenuPermission
     */
    public function addMenu(\Aspl\Bundle\MenuBundle\Entity\Menu $menus)
    {
        $this->menus[] = $menus;

        return $this;
    }

    /**
     * Remove menus
     *
     * @param \Aspl\Bundle\MenuBundle\Entity\Menu $menus
     */
    public function removeMenu(\Aspl\Bundle\MenuBundle\Entity\Menu $menus)
    {
        $this->menus->removeElement($menus);
    }

    /**
     * Add permissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\Permission $permissions
     * @return RoleMenuPermission
     */
    public function addPermission(\Aspl\Bundle\UserBundle\Entity\Permission $permissions)
    {
        $this->permissions[] = $permissions;

        return $this;
    }

    /**
     * Remove permissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\Permission $permissions
     */
    public function removePermission(\Aspl\Bundle\UserBundle\Entity\Permission $permissions)
    {
        $this->permissions->removeElement($permissions);
    }
}
