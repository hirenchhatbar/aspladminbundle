<?php

namespace Aspl\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Permission
 *
 * @ORM\Table(name="permission")
 * @ORM\Entity(repositoryClass="Aspl\Bundle\UserBundle\Entity\PermissionRepository")
 * @UniqueEntity(fields="name", message="Please enter different name, this name already exists in database.")
 */
class Permission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="Aspl\Bundle\UserBundle\Entity\RoleMenuPermission", mappedBy="permissions")
     */
    private $roleMenuPermissions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Permission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Permission
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Permission
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roleMenuPermissions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add roleMenuPermissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions
     * @return Permission
     */
    public function addRoleMenuPermission(\Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions)
    {
        $this->roleMenuPermissions[] = $roleMenuPermissions;

        return $this;
    }

    /**
     * Remove roleMenuPermissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions
     */
    public function removeRoleMenuPermission(\Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions)
    {
        $this->roleMenuPermissions->removeElement($roleMenuPermissions);
    }

    /**
     * Get roleMenuPermissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoleMenuPermissions()
    {
        return $this->roleMenuPermissions;
    }
}
