<?php

namespace Aspl\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Security\Core\Role\Role as CoreRole;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="Aspl\Bundle\UserBundle\Entity\RoleRepository")
 * @UniqueEntity(fields="name", message="Please enter different name, this name already exists in database.")
 * @UniqueEntity(fields="role", message="Please enter different role, this role already exists in database.")
 */
class Role extends CoreRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     *
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="RoleMenuPermission", mappedBy="roles")
     */
    private $roleMenuPermissions;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Role
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Role
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Add users
     *
     * @param \Aspl\Bundle\UserBundle\Entity\User $users
     * @return Role
     */
    public function addUser(\Aspl\Bundle\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Aspl\Bundle\UserBundle\Entity\User $users
     */
    public function removeUser(\Aspl\Bundle\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add roleMenuPermissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions
     * @return Role
     */
    public function addRoleMenuPermission(\Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions)
    {
        $this->roleMenuPermissions[] = $roleMenuPermissions;

        return $this;
    }

    /**
     * Remove roleMenuPermissions
     *
     * @param \Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions
     */
    public function removeRoleMenuPermission(\Aspl\Bundle\UserBundle\Entity\RoleMenuPermission $roleMenuPermissions)
    {
        $this->roleMenuPermissions->removeElement($roleMenuPermissions);
    }

    /**
     * Get roleMenuPermissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoleMenuPermissions()
    {
        return $this->roleMenuPermissions;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Role
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
}
