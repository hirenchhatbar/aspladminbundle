<?php

namespace Aspl\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\Type as AssertType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstname', 'text', array(
                'required' => false,
                'constraints' => array(
                        new NotBlank(array('message' => 'Please enter firstname.')),
                )))
                ->add('lastname', 'text', array(
                        'required' => false,
                        'constraints' => array(
                                new NotBlank(array('message' => 'Please enter lastname.')),
                        )))
                        ->add('email', 'text', array(
                                'required' => false,
                                'constraints' => array(
                                        new NotBlank(array('message' => 'Please enter email.')),
                                        new Email(array(
                                                'message' => 'The email "{{ value }}" is not a valid email.',
                                                'checkMX' => true,
                                        )),
                                )))
                                ->add('password', 'repeated', array(
                                        'type' => 'password',
                                        'invalid_message' => 'The password fields must match.',
                                        'options' => array('attr' => array('class' => 'form-control'), 'always_empty' => false),
                                        'required' => false,
                                        'first_options'  => array(
                                                'label' => 'Password',
                                                'constraints' => $options['is_new'] ? array(
                                                        new NotBlank(array('message' => 'Please enter password.')),
                                                ) : array()),
                                        'second_options' => array(
                                                'label' => 'Repeat Password',
                                                'constraints' => $options['is_new'] ? array(
                                                        new NotBlank(array('message' => 'Please enter confirm password.')),
                                                ) : array()),
                                        'first_name' => 'password',
                                        'second_name' => 'confirm_password',

                                ))
                                ->add('roles', 'entity', array(
                                    'required' => false,
                                    'multiple' => true,
                                    'mapped'   => true,
                                    'class'    => 'UserBundle:Role',
                                    'property' => 'name',
                                    'constraints' => array(
                                        new Count(array('min' => 1, 'minMessage' => 'Please select at least one role.')),
                                     )))
                                ->add('status', 'choice', array(
                                        'required' => false,
                                        'choices' => array('1' => 'Active', '0' => 'Inactive'),
                                        'multiple' => false,
                                        'expanded' => true,
                                        'empty_value' => false,
                                        'constraints' => array(
                                                new NotBlank(array('message' => 'Please select status.')),
                                        )))
                                        ->add('save', 'submit')
                                        ->add('saveAndNew', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Aspl\Bundle\UserBundle\Entity\User',
                'is_new' => false,
        ))
        ->setRequired(array(
                'is_new',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aspl_bundle_userbundle_user';
    }
}