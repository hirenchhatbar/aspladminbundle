<?php

namespace Aspl\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class RoleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                                        'required' => false,
                                        'constraints' => array(
                                                            new NotBlank(array('message' => 'Please enter name.')),
                                                         )))
            ->add('role', 'text', array(
                                        'required' => false,
                                        'constraints' => array(
                                                            new NotBlank(array('message' => 'Please enter role.')),
                                                         )))
            ->add('save', 'submit')
            ->add('saveAndNew', 'submit');
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Aspl\Bundle\UserBundle\Entity\Role'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aspl_bundle_userbundle_role';
    }
}
