<?php

namespace Aspl\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoleMenuPermissionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['menus'] as $menu) {
            $builder->add(sprintf('menu%s', $menu->getId()), 'choice', array('expanded' => true, 'multiple' => true, 'choices' => array($menu->getId() => $menu->getCaption())));

            foreach ($options['permissions'] as $permission) {
                $builder->add(sprintf('permission%s%s', $menu->getId(), $permission->getId()), 'choice', array('expanded' => true, 'multiple' => true, 'choices' => array(sprintf('%s,%s', $menu->getId(), $permission->getId()) => $permission->getName())));
            }
        }

        $builder->add('save', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'menus',
            'permissions',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aspl_bundle_userbundle_rolemenupermission';
    }
}
