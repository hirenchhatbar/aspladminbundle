<?php
namespace Aspl\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstname', 'text', array(
                'required' => false,
        ))
        ->add('lastname', 'text', array(
                'required' => false,
        ))
        ->add('email', 'text', array(
                'required' => false,
        ))
        ->add('status', 'choice', array(
                'required' => false,
                'choices' => array('1' => 'Active', '0' => 'Inactive'),
                'multiple' => false,
                'expanded' => true,
                'empty_value' => false,
        ))
        ->add('search', 'submit')
        ->add('reset', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'search';
    }
}