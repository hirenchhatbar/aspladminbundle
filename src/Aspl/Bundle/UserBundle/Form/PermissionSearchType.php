<?php
namespace Aspl\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PermissionSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', 'text', array(
                'required' => false,
        ))
        ->add('search', 'submit')
        ->add('reset', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'search';
    }
}