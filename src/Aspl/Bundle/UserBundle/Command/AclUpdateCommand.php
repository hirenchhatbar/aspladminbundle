<?php
namespace Aspl\Bundle\UserBundle\Command;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AclUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('acl:update')
        ->setDescription('Updates acls of roles')
        ->addArgument('role', InputArgument::OPTIONAL, 'Role to be updated')
        ->addOption('delete', null, InputOption::VALUE_NONE, 'If set, the task will remove role from ACL')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $roles = null;

        if ($input->getArgument('role')) {
            $roles = $this->getContainer()->get('doctrine')->getRepository('UserBundle:Role')->findByRole($input->getArgument('role'));
        }
        else {
            $roles = $this->getContainer()->get('doctrine')->getRepository('UserBundle:Role')->findAll();
        }

        foreach ($roles as $role) {
            $output->writeln(sprintf('Start role > %s', $role->getName()));

            if ($input->getOption('delete')) {
                $this->deleteRole($role->getRole());
            }
            else {
                $menus = $this->getContainer()->get('doctrine')->getRepository('MenuBundle:Menu')->getRoleMenus($role->getId());

                if ($menus) {
                    foreach ($menus as $menu) {
                        try {
                            $this->deleteAces($role->getRole(), $menu->getEntityClass());
                        }
                        catch (AclNotFoundException $e) {
                        }
                        catch (\Exception $e) {
                        }

                        $this->addAces(
                                $role->getRole(),
                                $menu->getEntityClass(),
                                $this->getMask($role->getId(), $menu->getId()));
                    }
                }
                else {
                    $output->writeln('No permission defined');
                }
            }

            $output->writeln(sprintf('End role > %s', $role->getName()));
            $output->writeln('');
        }

        $output->writeln('Role permission has been successfully updated');
    }

    protected function deleteAces($role, $entity)
    {
        try {
            $roleIdentity = new RoleSecurityIdentity($role);

            $aclProvider = $this->getContainer()->get('security.acl.provider');

            $acl = $aclProvider->findAcl(
                    new ObjectIdentity('class', $entity),
                    array($roleIdentity)
            );

            foreach ($acl->getClassAces() as $index => $ace) {
                $securityIdentity = $ace->getSecurityIdentity();
                if($securityIdentity->equals($roleIdentity)) {
                    $acl->deleteClassAce($index);
                }
            }

            $aclProvider->updateAcl($acl);
        } catch (AclNotFoundException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function addAces($role, $entity, $mask)
    {
        $roleIdentity = new RoleSecurityIdentity($role);

        $aclProvider = $this->getContainer()->get('security.acl.provider');

        $objectIdentity = new ObjectIdentity('class', $entity);

        try {
            $acl = $aclProvider->findAcl(
                    $objectIdentity,
                    array($roleIdentity)
            );
        }
        catch (\Exception $e) {
            $acl = $aclProvider->createAcl($objectIdentity);
        }

        $acl->insertClassAce($roleIdentity, $mask);

        $aclProvider->updateAcl($acl);
    }

    protected function getMask($roleId, $menuId)
    {
        $builder = new MaskBuilder();

        $permissions = $this->getContainer()->get('doctrine')->getRepository('UserBundle:Permission')->getPermissionByRoleAndMenu($roleId, $menuId);

        foreach ($permissions as $permission) {
            $builder->add(strtolower($permission->getName()));
        }

        return $builder->get();
    }

    protected function deleteRole($role)
    {
        $aclProvider = $this->getContainer()->get('security.acl.provider');

        $aclProvider->deleteSecurityIdentity(new RoleSecurityIdentity($role));
    }
}