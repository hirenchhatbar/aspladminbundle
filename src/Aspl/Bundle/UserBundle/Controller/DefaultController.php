<?php

namespace Aspl\Bundle\UserBundle\Controller;

use Symfony\Component\Finder\Exception\AccessDeniedException;

use Aspl\Bundle\UserBundle\Form\UserSearchType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Aspl\Bundle\UserBundle\Entity\User;
use Aspl\Bundle\UserBundle\Form\UserType;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\SecurityContextInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="_index", host="%host_admin%")
     * @Route("/login", name="_login", host="%host_admin%")
     * @Template("UserBundle:Default:login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $error   = null;

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $securityContext = $this->container->get('security.context');
            if( $securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
                return $this->redirect($this->generateUrl('_dashboard'));
            }
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        if ($error) {
            $this->get('message.manager')->setErrorMessage(7);
        }

        return $this->render(
                'UserBundle:Default:login.html.twig',
                array( 'last_username' => $lastUsername)
        );
    }

    /**
     * @Route("/login_check", name="_login_check", host="%host_admin%")
     * @Template()
     */
    public function loginCheckAction(Request $request)
    {
    }

    /**
     * @Route("/logout", name="_logout", host="%host_admin%")
     * @Template()
     */
    public function logoutAction()
    {
    }

    /**
     * @Route("/users", name="_user", host="%host_admin%")
     * @Template()
     */
    public function indexAction()
    {
        try {
            $this->get('acl.manager')->isGrantedToView();

            $this->get('list.manager')->init($this->getDoctrine()->getRepository('UserBundle:User'));

            $form = $this->createForm(new UserSearchType(), null, array(
                    'action' => $this->generateUrl('_user'),
            ));

            $data = $this->get('list.manager')->getData();

            if ($data['search'])
                $form->submit($data['search']);

            return array(
                    'heading' => 'Users',
                    'form' => $form->createView(),
                    'pagerfanta' => $this->get('list.manager')->getPagerfanta(),
                    'sorter' => $data['sorter'],
            );
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_dashboard'));
        }
    }

    /**
     * @Route("/user/new", name="_user_new", host="%host_admin%")
     * @Template("UserBundle:Default:form.html.twig")
     */
    public function newAction(Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToCreate();

            $form = $this->createForm(new UserType(), new User());

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setAddMessage('User');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_user_new' : '_user'));
            }

            return array('form' => $form->createView(), 'heading' => 'New user');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_user'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_user'));
        }
    }

    /**
     * @Route("/user/edit/{id}", name="_user_edit", host="%host_admin%")
     * @Template("UserBundle:Default:form.html.twig")
     */
    public function editAction($id, Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToEdit();

            $user = $this->getDoctrine()
            ->getRepository('UserBundle:User')
            ->find($id);

            if (!$user) {
                throw $this->createNotFoundException(
                        'No user found for id '.$id
                );
            }

            $form = $this->createForm(new UserType(), $user, array('is_new' => false));

            if ($this->_save($form, $request, $user->getPassword())) {
                $this->get('message.manager')->setSaveMessage('User');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_user_new' : '_user'));
            }

            return array('form' => $form->createView(), 'heading' => 'Edit user');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_user'));
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('User');

            return $this->redirect($this->generateUrl('_user'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_user'));
        }
    }

    /**
     * @Route("/user/delete/{id}", name="_user_delete", host="%host_admin%")
     * @Template()
     */
    public function deleteAction($id)
    {
        $deleted = false;

        try {
            $this->get('acl.manager')->isGrantedToDelete();

            $user = $this->getDoctrine()
            ->getRepository('UserBundle:User')
            ->find($id);

            if (!$user) {
                throw $this->createNotFoundException(
                        'No user found for id '.$id
                );
            }

            $this->_delete($user);

            $this->get('message.manager')->setDeleteMessage('User');

            $deleted = true;
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('User');
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();
        }

        return $this->redirect($this->generateUrl('_user', $deleted ? array('deleted' => true) : array()));
    }

    protected function _save(\Symfony\Component\Form\Form $form, Request $request, $originalPassword=null)
    {
        try {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user = $form->getData();

                $em = $this->getDoctrine()->getManager();

                if (false == $user->getPassword() && $originalPassword) {
                    $user->setPassword($originalPassword);
                }

                $em->persist($user);

                $em->flush();

                return TRUE;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _delete(User $user)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }
        catch (\Exception $e) {
            throw $e;
        }
    }
}