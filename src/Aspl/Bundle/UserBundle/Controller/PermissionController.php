<?php

namespace Aspl\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Aspl\Bundle\UserBundle\Form\PermissionType;
use Aspl\Bundle\UserBundle\Form\PermissionSearchType;
use Aspl\Bundle\UserBundle\Entity\Permission;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Finder\Exception\AccessDeniedException;

class PermissionController extends Controller
{
    /**
     * @Route("/permissions", name="_permission", host="%host_admin%")
     * @Template()
     */
    public function indexAction()
    {
        try {
            $this->get('acl.manager')->isGrantedToView();

            $this->get('list.manager')->init($this->getDoctrine()->getRepository('UserBundle:Permission'));

            $form = $this->createForm(new PermissionSearchType(), null, array(
                    'action' => $this->generateUrl('_permission'),
            ));

            $data = $this->get('list.manager')->getData();

            if ($data['search'])
                $form->submit($data['search']);

            return array(
                    'heading' => 'Permissions',
                    'form' => $form->createView(),
                    'pagerfanta' => $this->get('list.manager')->getPagerfanta(),
                    'sorter' => $data['sorter'],
            );
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_dashboard'));
        }
    }

    /**
     * @Route("/permission/new", name="_permission_new", host="%host_admin%")
     * @Template("UserBundle:Permission:form.html.twig")
     */
    public function newAction(Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToCreate();

            $form = $this->createForm(new PermissionType(), new Permission());

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setAddMessage('Permission');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_permission_new' : '_permission'));
            }

            return array('form' => $form->createView(), 'heading' => 'New permission');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_permission'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_permission'));
        }
    }

    /**
     * @Route("/permission/edit/{id}", name="_permission_edit", host="%host_admin%")
     * @Template("UserBundle:Permission:form.html.twig")
     */
    public function editAction($id, Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToEdit();

            $permission = $this->getDoctrine()
            ->getRepository('UserBundle:Permission')
            ->find($id);

            if (!$permission) {
                throw $this->createNotFoundException(
                        'No permission found for id '.$id
                );
            }

            $form = $this->createForm(new PermissionType(), $permission);

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setSaveMessage('Permission');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_permission_new' : '_permission'));
            }

            return array('form' => $form->createView(), 'heading' => 'Edit permission');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_permission'));
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Permission');

            return $this->redirect($this->generateUrl('_permission'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_permission'));
        }
    }

    /**
     * @Route("/permission/delete/{id}", name="_permission_delete", host="%host_admin%")
     * @Template()
     */
    public function deleteAction($id)
    {
        $deleted = false;

        try {
            $this->get('acl.manager')->isGrantedToDelete();

            $permission = $this->getDoctrine()
            ->getRepository('UserBundle:Permission')
            ->find($id);

            if (!$permission) {
                throw $this->createNotFoundException(
                        'No permission found for id '.$id
                );
            }

            $this->_delete($permission);

            $this->get('message.manager')->setDeleteMessage('Permission');

            $deleted = true;
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Permission');
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();
        }

        return $this->redirect($this->generateUrl('_permission', $deleted ? array('deleted' => true) : array()));
    }

    protected function _save(\Symfony\Component\Form\Form $form, Request $request)
    {
        try {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $permission = $form->getData();

                $em = $this->getDoctrine()->getManager();

                $em->persist($permission);

                $em->flush();

                return TRUE;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _delete(Permission $permission)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($permission);
            $em->flush();
        }
        catch (\Exception $e) {
            throw $e;
        }
    }
}