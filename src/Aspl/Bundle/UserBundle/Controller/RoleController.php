<?php

namespace Aspl\Bundle\UserBundle\Controller;

use Symfony\Component\Console\Output\NullOutput;

use Symfony\Component\Console\Input\ArrayInput;

use Aspl\Bundle\UserBundle\Command\AclUpdateCommand;

use Aspl\Bundle\UserBundle\Form\RoleMenuPermissionType;

use Aspl\Bundle\UserBundle\Form\RoleType;
use Aspl\Bundle\UserBundle\Form\RoleSearchType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Aspl\Bundle\UserBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Finder\Exception\AccessDeniedException;

class RoleController extends Controller
{
    /**
     * @Route("/roles", name="_role", host="%host_admin%")
     * @Template()
     */
    public function indexAction()
    {
        try {
            $this->get('acl.manager')->isGrantedToView();

            $this->get('list.manager')->init($this->getDoctrine()->getRepository('UserBundle:Role'));

            $form = $this->createForm(new RoleSearchType(), null, array(
                    'action' => $this->generateUrl('_role'),
            ));

            $data = $this->get('list.manager')->getData();

            if ($data['search'])
                $form->submit($data['search']);

            return array(
                    'heading' => 'Roles',
                    'form' => $form->createView(),
                    'pagerfanta' => $this->get('list.manager')->getPagerfanta(),
                    'sorter' => $data['sorter'],
            );
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_dashboard'));
        }
    }

    /**
     * @Route("/role/new", name="_role_new", host="%host_admin%")
     * @Template("UserBundle:Role:form.html.twig")
     */
    public function newAction(Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToCreate();

            $form = $this->createForm(new RoleType(), new Role());

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setAddMessage('Role');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_role_new' : '_role'));
            }

            return array('form' => $form->createView(), 'heading' => 'New role');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
    }

    /**
     * @Route("/role/edit/{id}", name="_role_edit", host="%host_admin%")
     * @Template("UserBundle:Role:form.html.twig")
     */
    public function editAction($id, Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToEdit();

            $role = $this->getDoctrine()
            ->getRepository('UserBundle:Role')
            ->find($id);

            if (!$role) {
                throw $this->createNotFoundException(
                        'No role found for id '.$id
                );
            }

            $form = $this->createForm(new RoleType(), $role);

            if ($this->_save($form, $request)) {
                $this->get('message.manager')->setSaveMessage('Role');

                return $this->redirect($this->generateUrl($form->get('saveAndNew')->isClicked() ? '_role_new' : '_role'));
            }

            return array('form' => $form->createView(), 'heading' => 'Edit role');
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Role');

            return $this->redirect($this->generateUrl('_role'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
    }

    /**
     * @Route("/role/delete/{id}", name="_role_delete", host="%host_admin%")
     * @Template()
     */
    public function deleteAction($id)
    {
        $deleted = false;

        try {
            $this->get('acl.manager')->isGrantedToDelete();

            $role = $this->getDoctrine()
            ->getRepository('UserBundle:Role')
            ->find($id);

            if (!$role) {
                throw $this->createNotFoundException(
                        'No role found for id '.$id
                );
            }

            $this->_delete($role);

            $this->get('message.manager')->setDeleteMessage('Role');

            $deleted = true;
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();
        }
        catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            $this->get('message.manager')->setNotFoundMessage('Role');
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();
        }

        return $this->redirect($this->generateUrl('_role', $deleted ? array('deleted' => true) : array()));
    }

    /**
     * @Route("/role/permission/{id}", name="_role_permission", host="%host_admin%")
     * @Template("UserBundle:Role:permission.html.twig")
     */
    public function permissionAction($id, Request $request)
    {
        try {
            $this->get('acl.manager')->isGrantedToEdit();

            $role = $this->getDoctrine()
            ->getRepository('UserBundle:Role')
            ->find($id);

            if (!$role) {
                throw $this->createNotFoundException(
                        'No role found for id '.$id
                );
            }

            $permissions = $this->getDoctrine()
            ->getRepository('UserBundle:Permission')
            ->findAll();

            if (!$permissions) {
                throw $this->createNotFoundException(
                        'No permissions found'
                );
            }

            $menus = $this->getDoctrine()->getRepository('MenuBundle:Menu')->getMenuwithEntityClass();

            $form = $this->createForm(new RoleMenuPermissionType(), null, array('menus' => $menus, 'permissions' => $permissions));

            if ($role->getData())
                $form->setData(unserialize($role->getData()));

            if ($this->_savePermission($role, $form, $request)) {
                $this->get('message.manager')->setSaveMessage('Role permission');

                return $this->redirect($this->generateUrl('_role'));
            }

            return array('form' => $form->createView(), 'menus' => $menus, 'permissions' => $permissions, 'heading' => sprintf('Role Permission - %s', $role->getName()));
        }
        catch (AccessDeniedException $e) {
            $this->get('message.manager')->setAccessDeniedMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
        catch (\Exception $e) {
            $this->get('message.manager')->setExceptionMessage();

            return $this->redirect($this->generateUrl('_role'));
        }
    }

    protected function _save(\Symfony\Component\Form\Form $form, Request $request)
    {
        try {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $role = $form->getData();

                $em = $this->getDoctrine()->getManager();

                $em->persist($role);

                $em->flush();

                return true;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _delete(Role $role)
    {
        try {
            // Remove acl
            $this->_removeAcl();

            // Remove role
            $em = $this->getDoctrine()->getManager();
            $em->remove($role);
            $em->flush();

            // Generate acl
            $this->_generateAcl();
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _savePermission($role, \Symfony\Component\Form\Form $form, Request $request)
    {
        try {
            $form->handleRequest($request);

            if ($form->isValid()) {
                // Save form for later user in edit
                $role->setData(serialize($form->getData()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($role);
                $em->flush();

                // Save permisson in database
                $roleMenuPermissions = $request->get($form->getName(), array());

                $this->getDoctrine()->getRepository('UserBundle:RoleMenuPermission')->savePermission($role->getId(), $roleMenuPermissions);

                // Remove acl
                $this->_removeAcl();

                // Generate acl
                $this->_generateAcl();

                return true;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    protected function _generateAcl()
    {
        $aclUpdateCommand = new AclUpdateCommand();
        $aclUpdateCommand->setContainer($this->container);
        $aclUpdateCommand->run(new ArrayInput(array()), new NullOutput());
    }

    protected function _removeAcl()
    {
        $aclUpdateCommand = new AclUpdateCommand();
        $aclUpdateCommand->setContainer($this->container);
        $aclUpdateCommand->run(new ArrayInput(array('--delete' => true)), new NullOutput());
    }
}