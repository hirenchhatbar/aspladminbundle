<?php
namespace Aspl\Bundle\UserBundle\Handler;

use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use Symfony\Component\Finder\Exception\AccessDeniedException;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AclManager
{
    const PERMISSION_VIEW   = 'VIEW';
    const PERMISSION_CREATE = 'CREATE';
    const PERMISSION_EDIT   = 'EDIT';
    const PERMISSION_DELETE = 'DELETE';

    protected $requestStack;
    protected $securityContext;
    protected $em;

    public function __construct(RequestStack $requestStack, SecurityContextInterface $securityContext, ObjectManager $em)
    {
        $this->requestStack = $requestStack;
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    protected function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    protected function getSecurityContext()
    {
        return $this->securityContext;
    }

    protected function getEntityManager()
    {
        return $this->em;
    }

    protected function getCurrentRoute()
    {
        return $this->getRequest()->get('_route');
    }

    protected function isGranted($permission)
    {
        $objectIdentity = new ObjectIdentity('class', $this->getFqdn());

        if (false === $this->getSecurityContext()->isGranted($permission, $objectIdentity)) {
            throw new AccessDeniedException();
        }
    }

    public function isGrantedToView()
    {
        try {
            $this->isGranted(self::PERMISSION_VIEW);
        }
        catch (AccessDeniedException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function isGrantedToCreate()
    {
        try {
            $this->isGranted(self::PERMISSION_CREATE);
        }
        catch (AccessDeniedException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function isGrantedToEdit()
    {
        try {
            $this->isGranted(self::PERMISSION_EDIT);
        }
        catch (AccessDeniedException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function isGrantedToDelete()
    {
        try {
            $this->isGranted(self::PERMISSION_DELETE);
        }
        catch (AccessDeniedException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function getFqdn()
    {
        $menu = $this->getEntityManager()
        ->getRepository('MenuBundle:Menu')
        ->findOneByLink($this->getCurrentRoute());

        $fqdn = $menu->getEntityClass();

        if (false == $fqdn && $menu->getParent())
            $fqdn = $menu->getParent()->getEntityClass();

        return $fqdn;
    }
}