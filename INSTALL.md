AsplAdminBundle
=======================

This bundle has a ready - startup admin based on which backend admin as per requirement can be made easily.

Installation
------------

### Step 1: Add below in your composer.json file
```
"require": {
    "aspl/admin-bundle": "dev-master"
},
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/hirenchhatbar/aspladminbundle.git"
    }
],
```

Now update composer:

``` bash
$ composer update
```

### Step 2: Configure gdemo/doctrine-extensions as mentioned in below URL:

https://github.com/l3pp4rd/DoctrineExtensions/blob/master/doc/symfony2.md

### Step 3: Load bundles

Add below in  in app/AppKernel.php:

``` php
<?php
//  app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
        new Aspl\Bundle\AdminBundle\AdminBundle(),
        new Aspl\Bundle\MenuBundle\MenuBundle(),
        new Aspl\Bundle\UserBundle\UserBundle(),
    );
    .......
}
```

### Step 3: Run symfony commands

Run below commands in console:

``` bash
$ php app/console cache:clear
$ php app/console cache:clear --env=prod
$ php app/console assets:install
$ php app/console init:acl
$ php app/console doctrine:schema:update --force
$ mysql -u<DATABASE_USER> -p<DATBASE_PASSWORD> <DATABASE_NAME> < vendor/aspl/admin-bundle/src/Aspl/Bundle/AdminBundle/DataFixtures/SQL/Sample.sql
$ php app/console acl:update
```

### Step 4: Load routing

Set below in your app routing file

``` yml
// app/config/routing.yml

_admin:
    resource: "@AdminBundle/Controller/DefaultController.php"
    type:     annotation
    prefix:   /
_user:
    resource: "@UserBundle/Controller/DefaultController.php"
    type:     annotation
    prefix:   /
_role:
    resource: "@UserBundle/Controller/RoleController.php"
    type:     annotation
    prefix:   /
_permission:
    resource: "@UserBundle/Controller/PermissionController.php"
    type:     annotation
    prefix:   /
_menu:
    resource: "@MenuBundle/Controller/DefaultController.php"
    type:     annotation
    prefix:   /
```

### Step 4: Set security settings

Set below in your app security file

``` yml
// app/config/security.yml

security:
    providers:
         main:
            entity:
                class: Aspl\Bundle\UserBundle\Entity\User
                property: username

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        secured_area:
            pattern:   ^/
            anonymous: ~
            form_login:
                login_path: _index
                check_path: _login_check
            logout:
                path:   /logout
                target: /
    encoders:
        Aspl\Bundle\UserBundle\Entity\User: plaintext
    acl:
        connection: default
```

### Step 5: Set admin host in your main bundle's services.xml

``` xml
// YOUR_BUNDLE/Resources/config/services.xml

<parameters>
    <parameter key="host_admin">ADMIN_HOST</parameter>
</parameters>
```

Now test
-------

Now run admin with user credentials "hiren@aspl.in/test". Mail me at hiren@aspl.in if you face any issue with installation.
